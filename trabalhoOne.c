/*Seu trabalho é criar um programa, escrito na linguagem C, que leia
arquivos de instância no formato acima, processe as informações e retorne
quais os dois pontos correspondentes aos lotes que os irmãos deverão comprar
para contruir suas empresas, de maneira que a distˆancia entre as duas seja
mínima, ---> instancia no drive 'estruturada' <---*/
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

void distanciaPonto(double x1,double y1, double x2, double y2, double *distancia){
    *distancia = sqrt(pow((x1-x2),2) + pow((y1-y2),2));
}

void menorDistancia(double mat[][2], int linhas){
    int i, j, first=0,troca=0;
    double pontoA[2],pontoB[2];
    double x1,y1,x2,y2,distancia,menorDist;
    
    for(i=linhas-1;(i>=1);i--){
        for(j=0;j<i;j++){
            x1=mat[i][0];
            y1=mat[i][1];
            x2=mat[j][0];
            y2=mat[j][1];
            
            distanciaPonto(x1,y1,x2,y2,&distancia);
            if(!first){
                menorDist = distancia;
                first++;
            }
            if(distancia<menorDist){
                menorDist = distancia;
                pontoA[0] = x1;
                pontoA[1] = y1;
                pontoB[0] = x2;
                pontoB[1] = y2;
            }
        }        
    }
    printf("\nPontos mais próximos: (%.15lf, %.15lf) e (%.15lf, %.15lf)"
            "\nDistância: %.2lfm\n", 
            pontoA[0], pontoA[1], pontoB[0], pontoB[1], distancia);
}

void lerArquivo(){
    int tamanho, i, j,cont=0;
    double lugar1=0, lugar2=0;
    FILE *arq;
	
    arq = fopen("instancia_100.txt", "r");
    
    if(arq == NULL) printf("\nERRO! \nArquivo inexistente\n");	
    else{
    	fscanf(arq, "%d\n", &tamanho);
    	double mat[tamanho][2];
	    for(i=0;i<tamanho;i++){	
	        fscanf(arq, "(%lf, %lf)\n", &lugar1, &lugar2);
	        mat[i][0]=lugar1;
	        mat[i][1]=lugar2;
	    }
	    menorDistancia(mat, tamanho);
		fclose(arq);
    }        
}

int main() {    
    lerArquivo();    
    return 0;
}